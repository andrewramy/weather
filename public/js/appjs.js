console.log('client server is running');



const weatherForm = document.querySelector('form');
const search = document.querySelector('input');
const mes0 = document.querySelector('#mes0');
const mes1 = document.querySelector('#mes1');
const mes2 = document.querySelector('#mes2');
const mes3 = document.querySelector('#mes3');
const img = document.querySelector('#img');




weatherForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const location = search.value;
    fetch('/weather?location=' + location).then((response) => {
        response.json().then((data) => {
            mes0.textContent = "";
            mes1.textContent = "";
            mes2.textContent = "";
            mes3.textContent = "";
            img.innerHTML = "";
            if (data.error) {
                mes3.textContent = data.error;
            } else {
                mes0.textContent = "city: " + data.city;
                mes1.textContent = "temp: " + data.temp;
                mes2.textContent = "weather: " + data.cloud;
                const imgURL = data.img.value;
                img.innerHTML="<img src="+data.img+" />"

                // let p = document.createElement("img");
                // img.append(p);
                // const z = document.querySelector('#img img');
                // z.setAttribute("src", imgURL);
                // console.log(imgURL);


            }
        })
    });
})