const path = require('path');
const express = require('express');
const request = require('request');
const hbs = require('hbs');

const app = express();
const port = process.env.PORT || 3000;
const publicPath = path.join(__dirname, '/public');
const partialPath = path.join(__dirname, '/partials');

const geocode = (location, callback) => {
    // const url = 'http://api.worldweatheronline.com/premium/v1/weather.ashx?key=dd0293e0f06c4a5e809164704203004&q=' + location + '&format=json&num_of_days=1&date=today';
    
    
    const url = 'http://api.weatherapi.com/v1/current.json?key=117feade3c194c4e958131017210403&q=' + location + '&aqi=no'


    request({ url: url }, (error, response) => {
        const data = JSON.parse(response.body);
        if (data.error) {
            callback({ error: data.error.message });
        } else {

            callback({ temp: data.current.temp_c, cloud: data.current.condition.text, img: data.current.condition.icon,city:data.location.name });
            
        }
    })
}

//geocode('cairo', (data, error) => {
//   console.log('Error', error);
//  console.log('Data', data);
//});

app.set('view engine', 'hbs');
app.use(express.static(publicPath));
hbs.registerPartials(partialPath);

app.get('', (req, res) => {
    res.render('index')
});

app.get('/help', (req, res) => {
    res.send('hello help')
});

app.get('/weather', (req, res) => {
    if (!req.query.location) {
        return res.send({
            error: 'you must provide an address'
        })
    }
    geocode(req.query.location, (data, error) => {
        return res.send(data);
        return res.send('Data');
    });
});

app.get('*', (req, res) => {
    res.send('this page could not found')
});



app.listen(port, () => {
    console.log('server is up on port ' + port);
});